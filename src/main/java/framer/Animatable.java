/*
Copyright (c) 2012, Edwin Jakobs
Copyright (c) 2013, Edwin Jakobs
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package framer;

import framer.easing.*;

import java.lang.reflect.Field;
import java.util.*;

/**
 * The Animatable base class.
 *
 * Example usage:
 * <pre>
 * class Foo extends Animatable {
 *      double x;
 *      double y;
 * }
 * </pre>
 * Then construct a {@code Foo} object and cue an animation
 * <pre>
 * Foo foo = new Foo();
 * foo.animate("x", 5, 1000).animate("y", 10, 1000);
 * </pre>
 * In your animation loop you update the object's animation and use its animated fields:
 * <pre>
 * foo.updateAnimation();
 * drawFoo(foo.x, foo.y);
 * </pre>

 * @author Edwin Jakobs
 */
@SuppressWarnings({"UnusedDeclaration", "JavaDoc"})
public class Animatable<T extends Animatable> {


    static Clock clock = new DefaultClock();

    /**
     * Globally sets the clock object to use. The default is a {@code DefaultClock} instance
     * @param clock a {@code a Clock instance}
     */
    public static void clock(Clock clock) {
        Animatable.clock = clock;
    }

    /**
     * Returns the global clock object
     * @return this
     */
    public static Clock clock() {
        return Animatable.clock;
    }


    long createAtTime = clock.time(); //System.currentTimeMillis();
    long lastTime = createAtTime;

    private ArrayList<AnimationKey> animationKeys = new ArrayList<AnimationKey>(10);

    private String stage = null;

    private Stack<Long> timeStack;

    private ArrayList<EmitListener> emitListeners;// = new ArrayList<EmitListener>();

    private Object animatable;


    T this_() {
        //noinspection unchecked
        return (T)this;
    }
    public Animatable(Object target) {
        animatable = target;
        animationKeys.ensureCapacity(10);
    }

    public Animatable() {
        animationKeys.ensureCapacity(10);
        animatable = this;
    }

    public Animatable(long createAtTime) {
        this.createAtTime = createAtTime;
        animationKeys.ensureCapacity(10);
        animatable = this;
    }

    public T pushTime() {
        if (timeStack == null) {
            timeStack = new Stack<Long>();
        }
        timeStack.push(createAtTime);
        return this_();
    }

    public T popTime() {
        createAtTime = timeStack.pop();
        return this_();
    }

    public T listen(EmitListener listener) {
        if (emitListeners == null) {
            emitListeners = new ArrayList<EmitListener>();
        }
        emitListeners.add(listener);
        return this_();
    }

    /**
     * Wait for a given time before cueing the next animation.
     * @param duration the delay in milliseconds
     * @return {@code this} for easy animation chaining
     */
    public T delay(long duration) {
        createAtTime += duration;
        return this_();
    }

    /**
     * Wait until a the given time
     * @param time the time in milliseconds
     * @return {@code this} for easy animation chaining
     */
    public T waitUntil(long time) {
        createAtTime = time;
        return this_();
    }


    public String stage() {
        return stage;
    }

    /**
     * Queues a stage
     * @param stage the name of the stage to queue
     * @return {@code this} for easy animation chaining
     */
    public Animatable stage(String stage) {
        AnimationKey key = new AnimationKey(null, 0, 0, createAtTime);
        key.stage = stage;
        animationKeys.add(key);

        return this;

    }

    public T emit(Object message) {
        /**
         * Queues a message emission
         */
        AnimationKey key = new AnimationKey(null, 0, 0, createAtTime);
        key.message = message;
        animationKeys.add(key);

        return this_();

    }


    /**
     * Animates a single variable.
     * @param variable the name of the variable to animate
     * @param target the target value
     * @param duration the duration of the animation in milliseconds
     * @return {@code this} for easy animation chaining
     */
    public T animate(String variable, double target, long duration) {
        return animate(variable, target, duration, Easing.None);
    }

    /**
     * Animates a single variable.
     * @param variable the name of the variable to animate
     * @param target the target value
     * @param duration the duration of the animation in milliseconds
     * @param easing the easing to use during the animation
     * @return {@code this} for easy animation chaining
     * Also arrays of floats or doubles can be animated
     * <pre>animatable.animate("x[0]", 100, 1000).complete().animate("x[0]", 0, 1000);</pre>
     */
    public T animate(String variable, double target, long duration, Easing easing) {
        return animate(variable, target, duration, easing.easer);
    }

    /**
     * Animates a single variable.
     * @param variable the name of the variable to animate
     * @param target the target value
     * @param duration the duration of the animation in milliseconds
     * @param easer the easing to use during the animation
     * @return {@code this} for easy animation chaining
     * Also arrays of floats or doubles can be animated
     * <pre>animatable.animate("x[0]", 100, 1000).complete().animate("x[0]", 0, 1000);</pre>
     */
    public T animate(String variable, double target, long duration, Easer easer) {
        AnimationKey key = new AnimationKey(variable, target, duration, createAtTime);
        key.setEasing(easer);
        if (animationKeys == null) {
            animationKeys = new ArrayList<>();
        }
        animationKeys.add(key);
        return this_();
    }


    /**
     * Adds a progress callback to the last cued animation.
     * @param callback the callback during the progression of the animation
     * @return {@code this} for easy animation chaining
     */
    public T progress(AnimationProgressCallback callback) {
        if (animationKeys.size() > 0) {
            animationKeys.get(animationKeys.size()-1).addProgressCallback(callback);
        }
        return this_();
    }

    /**
     * Queries if animations are cued.
     * @return {@code true} iff animations are cued.
     */
    public boolean hasAnimations() {
        return animationKeys.size() != 0;
    }

    /**
     * Queries if animations matching any of the given variables are cued.
     * @param variables
     * @return {@code true} iff animations matching any of the given variables are cued.
     */
    public boolean hasAnimations(String ... variables) {
        Set<String> variableSet = new HashSet<>();
        for (String variable: variables) {
            variableSet.add(variable);
        }
        for (AnimationKey animationKey: animationKeys) {
            if (animationKey.variable != null) {
                if (variableSet.contains(animationKey.variable)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Cues an additive animation for a single variable.
     * @param variable the target animation
     * @param target the target value
     * @param duration the duration of the animation in milliseconds
     * @return {@code this} for easy animation chaining
     */
    public T add(String variable, double target, long duration) {
        return add(variable, target, duration, Easing.None);
    }

    /**
     * Cues an additive animation for a single variable using a given easing mode.
     * @param variable the target animation
     * @param target the target value
     * @param duration the duration of the animation in milliseconds
     * @param easing the easing to use for this animation
     * @return {@code this} for easy animation chaining
     * <p>Example usage:
     * <pre>
     * {@code
     * animatable.add("x", 100, 1000);
     * animatable.complete();
     * animatable.add("x", 0, 1000);}
     * </pre>
     */

    public T add(String variable, double target, long duration, Easing easing) {
        return add(variable, target, duration, easing.easer);
    }

    public T add(String variable, double target, long duration, Easer easer) {
        AnimationKey key = new AnimationKey(variable, target, duration, createAtTime);
        key.setAnimationMode(AnimationKey.AnimationMode.Additive);
        key.setEasing(easer);
        animationKeys.add(key);
        return this_();
    }


    /**
     * Wait for the last cued animation to complete before starting the next animation
     * <p>
     * Example usage:
     * <pre>
     * animatable.animate("x", 100, 1000);
     * animatable.complete();
     * animatable.animate("x", 0, 1000);}
     * </pre>
     * Or using chaining:
     * {@code animatable.animate("x", 100, 1000).complete().animate("x", 0, 1000);}
     *
     * @return {@code this} for easy animation chaining
     */
    public T complete() {
        return complete(this, null);
    }


    public T complete(String variable) {
        AnimationKey key = lastQueued(variable);
        if (key != null) {
            createAtTime = key.start + key.duration;

        }
        return this_();
    }

    /**
     * Wait for the last cued animation to complete before starting the next animation and install
     * a completion call back that is called when the last queued animation ends.
     * <p>Example usage:
     * <pre>
     * {@code
     * AnimationCompletedCallBack callback = new AnimationCompletedCallBack() {
     *     public void animationCompleted(Animatable target) {
     *         System.out.println("animation completed");
     *     }
     * }
     *
     * animatable.animate("x", 100, 1000);
     * animatable.complete(callback);
     * animatable.animate("x", 0, 1000);}
     * </pre>
     *
     * @param callback the callback to call when the animation is completed
     * @return {@code this} for easy animation chaining
     */
    public T complete(AnimationCompletedCallback callback) {
        return complete(this, callback);
    }


    /**
     * Moves the animation cursor {@code time} milliseconds before the completion of the last queued animation
     * <p>
     * This is equivalent to {@code .complete().delay(-time);}
     * @param time time in milliseconds before end of completion of last queued animation
     * @return this
     */
    public T beforeComplete(long time) {
        return beforeComplete(time, null);
    }


    public T beforeComplete(long time, AnimationCompletedCallback callback) {

        AnimationKey key = lastQueued();

        if (key != null) {
            if (callback != null) {
                key.addCheckpointCallback(key.start + key.duration - time, callback);
            }
            createAtTime = key.start + key.duration - time;
        }


        return this_();
    }

    /**
     * Moves the animation cursor {@code time} milliseconds after the last queued animation
     * @param time time in millis after start of last queud animation
     * @return this
     */
    public T afterStart(long time) {
        return afterStart(time, null);
    }

    public T afterStart(long time, AnimationCompletedCallback callback) {

        AnimationKey key = lastQueued();

        if (key != null) {
            if (callback != null) {
                key.addCheckpointCallback(key.start + time, callback);
            }
            createAtTime = key.start + time;
        }

        return this_();
    }


    /**
     * Sets the easing mode for the last cued animation.
     * <p>
     * Example usage:
     * <pre>
     * {@code animatable.animate("x", 100, 1000).ease(Easing.BounceOut);
     * animatable.complete();
     * animatable.animate("x", 0, 1000).ease(Easing.BounceIn);}
     * </pre>
     *
     * @param easing the easing mode to set
     * @return {@code this} for easy animation chaining
     */
    public T ease(Easing easing) {
        return ease(easing.easer);
    }

    public T ease(Easer easer) {
        AnimationKey last = lastQueued();
        if (last != null) {
            last.setEasing(easer);
        }
        return this_();
    }

    /**
     * Returns the current velocity of the animating variable
     * @param variable the variable to give the velocity for
     * @return velocity in units per second
     */

    public double velocity(String variable) {


        for (AnimationKey key: animationKeys) {
            if (key.start <= lastTime  && key.variable.equals(variable)) {

                double delta = key.target - key.from;
                long dt = lastTime - key.start;
                //dt /= key.duration;

                return key.getEasing().velocity(dt/1000.0, key.from, delta, key.duration/1000.0);

            }
        }
        return 0;
    }


    /**
     * Wait for the last cued animation in the given animatable to complete before starting the next animation.
     * @param animatable the animatable to wait for
     * @return {@code this} for easy animation chaining
     */
    public T complete(Animatable animatable) {
        //noinspection unchecked,RedundantCast
        return (T)complete(animatable, null);
    }


    /**
     * Wait for the animation to complete in the given animatable to complete before starting the next animation and install
     * a completion call back that is called when the last queued animation ends.
     * @param animatable the animatable to wait for
     * @param callback the callback to call when the animation is completed
     * @return {@code this} for easy animation chaining
     */

    public T complete(Animatable<T> animatable, AnimationCompletedCallback  callback) {

        AnimationKey waitForKey = animatable.lastQueued();

        if (waitForKey != null) {

            createAtTime = waitForKey.start + waitForKey.duration;

            if (callback != null)
                animationKeys.get(animationKeys.size()-1).addCompletionCallback(callback);
        }
        else {

            if (callback != null) {
                //noinspection unchecked
                callback.animationCompleted(this_());
            }

        }

        return this_();
    }

    /**
     * Cancels all animations.
     * @return {@code this} for easy animation chaining
     */
    public T cancel() {
        if (animationKeys != null) {
            animationKeys.clear();
        }
        createAtTime = lastTime;
        return this_();
    }

    /**
     * Cancels all queued animations. Running animations will run until end.
     * @return {@code this} for easy animation chaining
     */

    public T cancelQueued() {

        List<AnimationKey> keep = new ArrayList<AnimationKey>();

        for (AnimationKey key: animationKeys) {
            if (key.getAnimationState() == AnimationKey.AnimationState.Playing) {
                keep.add(key);
            }
        }

        animationKeys.clear();
        animationKeys.addAll(keep);

        return this_();
    }


    /**
     * Ends running animations and cancels all cued animations. The behaviour of end() differs from cancel() in how animated variables are treated, end() sets the animated variables to their target value. Using end() will produce a pop in the animation.
     * @return {@code this} for easy animation chaining
     */
    public T end() {

        for (AnimationKey key: animationKeys) {
            if (key.getAnimationState() == AnimationKey.AnimationState.Playing) {
                setFieldValue(key.variable, key.target);
            }
        }

        animationKeys.clear();
        createAtTime = lastTime;
        return this_();
    }

    /**
     * Cancels selected animations
     * <pre>
     * animatable.animate("x", 100, 1000);
     * animatable.animate("y", 100, 1000);
     * animatable.cancel(new String["x"]);
     * </pre>
     * @param fields the names of the fields for which animations should be cancelled
     * @return {@code this} for easy animation chaining
     */
    public T cancel(String[] fields) {

        HashSet<String> filter = new HashSet<String>();
        Collections.addAll(filter, fields);

        ArrayList<AnimationKey> toRemove = new ArrayList<AnimationKey>();
        for (AnimationKey key: animationKeys) {
            if (filter.contains(key.variable)) {
                toRemove.add(key);
            }
        }

        animationKeys.removeAll(toRemove);


        //animationKeys.clear();
        createAtTime = lastTime;
        return this_();
    }



    private Map<String, Field> fieldCache = new HashMap<>();
    private final Field getField(String variable) {

        Field field = fieldCache.get(variable);

        if (field == null) {
            if (animatable == null) {
                animatable = this;
            }
            Class _class = animatable.getClass();

            while (_class != null) {
                try {

                    field = _class.getDeclaredField(variable);
                    field.setAccessible(true);
                    fieldCache.put(variable, field);
                    return field;
                } catch (NoSuchFieldException e) {
                    _class = _class.getSuperclass();
                }
            }
        }
        return field;
    }

    public static String array(String variable, int index) {
        return variable+"[" + index +"]";
    }

    private final double getFieldValue(String variable) {
        try {

            final int indexPos = variable.charAt(variable.length()-1) == ']'? variable.indexOf("[") : -1;
            if (indexPos != -1) {

                final String fieldName = variable.substring(0,indexPos);
                final String keyName = variable.substring(indexPos+1, variable.length()-1);
                final Field field = getField(fieldName);

                final Class _class = field.getType();

                if (_class == double[].class) {
                    int offset = Integer.parseInt(keyName);
                    double[] a = (double[]) field.get(animatable);
                    return a[offset];
                }
                else if (_class == float[].class) {
                    int offset = Integer.parseInt(keyName);
                    float[] a = (float[]) field.get(animatable);
                    return a[offset];
                }
                else if (field.get(this) instanceof List) {
                    int offset = Integer.parseInt(keyName);
                    List a = (List) field.get(this);
                    Object o = a.get(offset);
                    if (o instanceof Double) {
                        return (Double) o;
                    } else if (o instanceof Float) {
                        return (Float) o;
                    }
                }
            }
            else {
                final Field field = getField(variable);
                if (field != null) {
                    return field.getDouble(animatable);
                }
            }
        } catch (SecurityException | IllegalAccessException | IllegalArgumentException e) {
            e.printStackTrace();
        }

        return 0;
    }


    private AnimationKey lastQueued() {
        if (animationKeys.size() > 0) {
            return animationKeys.get(animationKeys.size()-1);
        }
        else
            return null;
    }

    private AnimationKey lastQueued(String variable) {
        for (int i = animationKeys.size() - 1; i >= 0; --i) {
            AnimationKey k = animationKeys.get(i);
            if (variable.equals(k.getVariable())) {
                return k;
            }
        }
        return null;
    }

    private final void setFieldValue(final String variable, final double value) {

        try {

            final int indexPos = variable.charAt(variable.length()-1) == ']'? variable.indexOf("[") : -1;
            if (indexPos != -1) {

                final String fieldName = variable.substring(0,indexPos);
                final String keyName = variable.substring(indexPos+1, variable.length()-1);

                final Field field = getField(fieldName);
                final Class _class = field.getType();
                field.setAccessible(true);
                if (_class == double[].class) {
                    final int offset = Integer.parseInt(keyName);
                    final double[] a = (double[]) field.get(animatable);
                    a[offset] = value;
                } else if (_class == float[].class) {
                    final int offset = Integer.parseInt(keyName);
                    final float[] a = (float[]) field.get(animatable);
                    a[offset] = (float)value;
                } else if (field.get(animatable) instanceof List) {
                    final int offset = Integer.parseInt(keyName);
                    final List list = (List) field.get(animatable);
                    final Object o = list.get(offset);
                    if (o instanceof Double) {
                        //noinspection unchecked
                        list.set(offset, value);
                    }
                    else if (o instanceof Float) {
                        //noinspection unchecked
                        list.set(offset, (float)value);
                    }
                }
            }
            else {
                final Field field = getField(variable);

                if (field != null) {
                    if (field.getType() == double.class) {
                        field.setDouble(animatable, value);
                    } else if (field.getType() == float.class) {
                        field.setFloat(animatable, (float) value);
                    } else {
                        System.err.println("warning: could not set animatable value");
                    }
                }
            }
        } catch (SecurityException | IllegalAccessException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    private double blend(Easer easing, double dt, double from, double delta) {
        return easing.ease(dt, from, delta, 1);
    }


    /**
     * Returns the duration of the animation in milliseconds
     * @return the duration of the running plus queued animations
     */
    public long duration() {
        long latest = 0;
        for (AnimationKey key: this.animationKeys) {
            long end = key.start + key.duration;
            if (end > latest) {
                latest = end;
            }
        }

        return Math.max(0, Math.max(latest, createAtTime)-lastTime);
    }

    /**
     * Updates the animation state with the clock time
     */
    public void updateAnimation() {
        updateAnimation(clock.time());
    }

    /**
     * Updates the animation state with a user supplied time
     * @param time the time to use for updating the animation state
     */
    public void updateAnimation(long time) {

        final ArrayList<AnimationKey> toRemove = new ArrayList<>();
        final ArrayList<Call> calls = new ArrayList<>();

        String updateStage = null;


        if (animationKeys == null) {
            animationKeys = new ArrayList<>();
        }
        for (AnimationKey key: animationKeys) {

            if (key.start == -1) {
                key.start = time;
            }

            if (key.start <= time) { // && key.start + key.duration >= time) {

                if (key.getAnimationState() == AnimationKey.AnimationState.Queued) {

                    if (key.variable != null) {
                        key.play(getFieldValue(key.variable));
                    }
                    else {
                        key.play(0);
                    }
                }

                if (key.getAnimationState() == AnimationKey.AnimationState.Playing) {
                    double dt = (double) (time - key.start);

                    if (key.duration > 0) {
                        dt /= (double)key.duration;
                    }
                    else {
                        dt = 1;
                    }

                    if (dt < 0)
                        dt = 0;
                    if (dt >= 1) {
                        dt = 1;
                        key.stop();
                    }

                    for (AnimationCheckpoint checkpoint: key.checkpointCallbacks) {
                        if (checkpoint.time <= time && !checkpoint.reached) {
                            calls.add(new AnimationCompleteCall(checkpoint.callback, this));
                            checkpoint.reached = true;
                        }
                    }

                    for (AnimationProgressCallback callback: key.getProgressCallbacks()) {
                        calls.add(new AnimationProgressCall(callback, this, dt, key.duration));
                        //callback.animationProgress(this, dt, key.duration);
                    }

                    if (key.variable != null) {
                        if (key.getAnimationMode() == AnimationKey.AnimationMode.Blend) {
                            double value = blend(key.getEasing(), dt, key.from, key.target-key.from);
                            setFieldValue(key.variable, value);
                        }
                        else if (key.getAnimationMode() == AnimationKey.AnimationMode.Additive) {
                            double value = blend(key.getEasing(), dt, key.from, key.target);
                            setFieldValue(key.variable, value);
                        }
                    }

                    if (key.variable == null && key.stage != null) {
                        updateStage = key.stage;
                    }

                    if (key.variable == null && key.message != null) {

                        if (emitListeners != null) {
                            for (EmitListener listener : emitListeners) {
                                calls.add(new EmitListenerCall(listener, this, key.message));
                            }
                        }
                    }

                }

                if (key.getAnimationState() == AnimationKey.AnimationState.Stopped) {
                    for (AnimationCompletedCallback callback: key.getCompletionCallbacks()) {
                        //callback.animationCompleted(this);
                        calls.add(new AnimationCompleteCall(callback, this));
                    }
                    toRemove.add(key);
                }
            }

        }

        for (Call call: calls) {
            call.call();
        }

        if (updateStage != null) {
            stage = updateStage;
        }
        for (AnimationKey key: toRemove) {
            animationKeys.remove(key);
        }

        lastTime = time;

        if (lastTime > createAtTime) {
            createAtTime = lastTime;
        }
    }


    long createAtTime() {
        return createAtTime;
    }

    /**
     * Returns the number of playing plus queued animations
     * @return number of playing plus queued animations
     */
    public int animationCount() {
        return animationKeys.size();
    }
}
