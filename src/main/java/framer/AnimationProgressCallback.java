package framer;
/**
 * Callback interface to report animation progress.
 * @author Edwin Jakobs
 *
 */
public interface AnimationProgressCallback<T extends Animatable<T>> {
	/**
	 * called during updates of the animation
	 * @param target the animation target
	 * @param time a value ranging from 0 to 1 that indicates the progress of the animation
	 * @param duration the total duration of the animation (in milliseconds)
	 */
	public void animationProgress(Animatable<T> target, double time, double duration);
	
}
