package framer;

/**
 * Clock interface
 */
public interface Clock {
    public long time();
}
