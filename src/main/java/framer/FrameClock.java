package framer;

public class FrameClock implements Clock {

    double fps = 25.0;
    long frame = 0;

    public FrameClock fps(double fps) {
        this.fps = fps;
        return this;
    }

    public FrameClock next() {
        frame++;
        return this;
    }

    public long frame() {
        return frame;
    }

    public FrameClock frame(long frame) {
        this.frame = frame;
        return this;
    }

    @Override
    public long time() {
        return (long) (frame * fps);
    }
}
