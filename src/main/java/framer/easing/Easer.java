package framer.easing;

/**
 * Created by voorbeeld on 8/6/14.
 */
public interface Easer {
    public double velocity(double t, double b, double c, double d);
    public double ease(double t, double b, double c, double d);

}
