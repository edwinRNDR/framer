package framer.easing.impl;

import framer.easing.Easer;


public class CubicIn implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return (3 * c * t * t) / (d*d*d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return c * (t /= d) * t * t + b;
    }
}
