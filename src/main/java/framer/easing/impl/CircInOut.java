package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class CircInOut implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        if (t / (d /2) < 1) {
            return  (2* c * t)/((d * d) * Math.sqrt(1-(4 * (t * t))/(d * d)));
        }
        else {
            return -(2* c * t)/((d * d) * Math.sqrt(2-(4 * (t * t))/(d * d)));
        }
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        if ((t /= d / 2) < 1)
            return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
        return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
    }
}
