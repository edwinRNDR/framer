package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class ExpoInOut implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        if (t ==0) {
            return 0;
        }
        else if (t == d) {
            return 0;
        }
        else if (t / (d /2) < 1) {
            return (c * Math.log(32) * Math.pow(2,((20 * t)/ d -9)))/ d;
        }
        else {
            return (c * Math.log(32) * Math.pow(2,(11-(20 * t)/ d)))/ d;
        }
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        if (t == 0) {
            return b;
        }
        else if (t == d) {
            return b + c;
        }
        else if ((t /= d / 2) < 1) {
            return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        }
        else {
            return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
        }
    }
}
