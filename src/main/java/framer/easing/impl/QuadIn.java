package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class QuadIn implements Easer {


    @Override
    public double velocity(double t, double b, double c, double d) {
        return (2 * c * t) / (d * d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return c * (t /= d) * t + b;
    }
}
