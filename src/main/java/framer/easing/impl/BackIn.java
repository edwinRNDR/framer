package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class BackIn implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return (c * t *(8.10474* t -3.40316* d))/(d * d * d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        double s = 1.70158;
        return c * (t /= d) * t * ((s + 1) * t - s) + b;

        //return c * (t / d) * (t/d) * ((1.70158 + 1) * (t/d) - 1.70158) + b;
    }
}
