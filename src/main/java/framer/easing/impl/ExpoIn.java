package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class ExpoIn implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        if (t == 0) {
            return 0;
        }
        else {
            return (c * Math.log(32) * Math.pow(2,((10 * t)/ d -9)))/ d;
        }
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
    }
}
