package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class BounceInOut implements Easer {




    @Override
    public double velocity(double t, double b, double c, double d) {
        if (t < d /2) {
            double result;

            if ((d - t * 2) / d < 1.0/2.75) {
                result = (15.125 * c * (d - t * 2)) / (d * d);
            }
            else if ((d - t * 2) / d < 2.0/2.75) {
                result = (c * (15.125 * (d - t * 2) - 8.25 * d)) / (d * d);
            }
            else if ((d - t * 2) / d < 2.5/2.75f) {
                result = (c * (15.125 * (d - t * 2) - 12.375 * d)) / (d * d);
            }
            else {
                result = (c * (15.125 * (d - t * 2) - 14.4375 * d)) / (d * d);
            }
            return result * 0.5;
        }
        else {
            double result;

            if ((t * 2 - d) / d < 1.0/2.75) {
                result = (15.125 * c * (t * 2 - d)) / (d * d);
            }
            else if ((t * 2 - d) / d < 2.0/2.75) {
                result = (c * (15.125 * (t * 2 - d) - 8.25 * d)) / (d * d);
            }
            else if ((t * 2 - d) / d < 2.5/2.75f) {
                result = (c * (15.125 * (t * 2 - d) - 12.375 * d)) / (d * d);
            }
            else {
                result = (c * (15.125 * (t * 2 - d) - 14.4375 * d)) / (d * d);
            }
            return result * 0.5;
        }
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        double t1 = d - t * 2;
        double result;
        if ((t1 /= d) < (1 / 2.75f)) {
            result = c * (7.5625f * t1 * t1) + (double) 0;
        } else if (t1 < (2 / 2.75f)) {
            result = c * (7.5625f * (t1 -= (1.5f / 2.75f)) * t1 + .75f) + (double) 0;
        } else if (t1 < (2.5 / 2.75)) {
            result = c * (7.5625f * (t1 -= (2.25f / 2.75f)) * t1 + .9375f) + (double) 0;
        } else {
            result = c * (7.5625f * (t1 -= (2.625f / 2.75f)) * t1 + .984375f) + (double) 0;
//return c * (7.5625 * pow((t/d) -(2.625 / 2.75),2) + .984375) + b;

        }
        double t2 = t * 2 - d;
        double result1;
        if ((t2 /= d) < (1 / 2.75f)) {
            result1 = c * (7.5625f * t2 * t2) + (double) 0;
        } else if (t2 < (2 / 2.75f)) {
            result1 = c * (7.5625f * (t2 -= (1.5f / 2.75f)) * t2 + .75f) + (double) 0;
        } else if (t2 < (2.5 / 2.75)) {
            result1 = c * (7.5625f * (t2 -= (2.25f / 2.75f)) * t2 + .9375f) + (double) 0;
        } else {
            result1 = c * (7.5625f * (t2 -= (2.625f / 2.75f)) * t2 + .984375f) + (double) 0;
//return c * (7.5625 * pow((t/d) -(2.625 / 2.75),2) + .984375) + b;

        }
        if (t < d / 2)
            return (c - result + (double) 0) * .5f + b;
        else
            return result1 * .5f + c * .5f + b;
    }
}
