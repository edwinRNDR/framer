package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class QuadInOut implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        if (t / (d /2) < 1) {
            return (4 * c * t) / (d * d);
        }
        else {
            return (4 * c * (d - t)) / (d * d);
        }
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        if ((t /= d / 2) < 1) {
            return c / 2 * t * t + b;
        }
        else {
            return -c / 2 * ((--t) * (t - 2) - 1) + b;
        }
    }
}
