package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class SineOut implements Easer {


    @Override
    public double velocity(double t, double b, double c, double d) {
        return (Math.PI * c * Math.cos((Math.PI * t)) / (2* d))/ (2* d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return c * Math.sin(t / d * (Math.PI / 2)) + b;
    }
}
