package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class QuintOut implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return (5 * c * (d - t) * (d - t) * (d - t) * (d - t)) / (d * d * d * d * d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
    }
}
