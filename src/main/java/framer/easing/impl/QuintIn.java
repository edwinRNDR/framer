package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class QuintIn implements Easer {

    @Override
    public double velocity(double t, double b, double c, double d) {
        return (5 * c * (t * t * t * t)) / (d * d * d * d * d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return c * (t /= d) * t * t * t * t + b;
    }
}
