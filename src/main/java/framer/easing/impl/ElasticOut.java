package framer.easing.impl;

import framer.easing.Easer;

import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class ElasticOut implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return (pow(2,(-(10* t)/ d)) * (20.944 * c * cos(1.5708-(20.944 * t)/ d)-6.93147 * c * sin((20.944 * (t -0.075 * d))/ d)))/ d;
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        if (t == 0)
            return b;
        if ((t /= d) == 1)
            return b + c;
        double p = d * .3f;
        double a = c;
        double s = p / 4;
        return (a * pow(2, -10 * t) * sin((t * d - s) * (2 * Math.PI) / p) + c + b);

    }
}
