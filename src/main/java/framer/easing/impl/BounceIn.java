package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class BounceIn implements Easer {

    public static double velocityOut(double t, double b, double c, double d) {

        if (t/d < 1.0/2.75) {
            return  (15.125 * c * t) / (d*d);
        }
        else if (t/d < 2.0/2.75) {
            return (c * (15.125*t-8.25*d))/(d*d);
        }
        else if (t/d < 2.5/2.75f) {
            return (c * (15.125*t-12.375*d))/(d*d);
        }
        else {
            return (c*(15.125*t-14.4375*d))/(d*d);
        }
    }



    @Override
    public double velocity(double t, double b, double c, double d) {
        return velocityOut(d - t, 0, c, d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        double t1 = d - t;
        double result;
        if ((t1 /= d) < (1 / 2.75f)) {
            result = c * (7.5625f * t1 * t1) + (double) 0;
        } else if (t1 < (2 / 2.75f)) {
            result = c * (7.5625f * (t1 -= (1.5f / 2.75f)) * t1 + .75f) + (double) 0;
        } else if (t1 < (2.5 / 2.75)) {
            result = c * (7.5625f * (t1 -= (2.25f / 2.75f)) * t1 + .9375f) + (double) 0;
        } else {
            result = c * (7.5625f * (t1 -= (2.625f / 2.75f)) * t1 + .984375f) + (double) 0;
//return c * (7.5625 * pow((t/d) -(2.625 / 2.75),2) + .984375) + b;

        }
        return c - result + b;
    }
}
