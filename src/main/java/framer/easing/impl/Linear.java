package framer.easing.impl;
import framer.easing.Easer;

public class Linear implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return c/d;
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return b + c * (t/d);
    }
}
