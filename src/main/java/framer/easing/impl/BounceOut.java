package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class BounceOut implements Easer {


    @Override
    public double velocity(double t, double b, double c, double d) {

        if (t / d < 1.0/2.75) {
            return  (15.125 * c * t) / (d * d);
        }
        else if (t / d < 2.0/2.75) {
            return (c * (15.125* t -8.25* d))/(d * d);
        }
        else if (t / d < 2.5/2.75f) {
            return (c * (15.125* t -12.375* d))/(d * d);
        }
        else {
            return (c *(15.125* t -14.4375* d))/(d * d);
        }
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        if ((t /= d) < (1 / 2.75f)) {
            return c * (7.5625f * t * t) + b;
        } else if (t < (2 / 2.75f)) {
            return c * (7.5625f * (t -= (1.5f / 2.75f)) * t + .75f) + b;
        } else if (t < (2.5 / 2.75)) {
            return c * (7.5625f * (t -= (2.25f / 2.75f)) * t + .9375f) + b;
        } else {
            return c * (7.5625f * (t -= (2.625f / 2.75f)) * t + .984375f) + b;
//return c * (7.5625 * pow((t/d) -(2.625 / 2.75),2) + .984375) + b;

        }
    }



}
