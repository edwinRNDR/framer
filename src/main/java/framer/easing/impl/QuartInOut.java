package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class QuartInOut implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        if (t / (d /2) < 1) {
            return (32 * c * t * t * t)/ (d * d * d * d);
        }
        else {
            return(32 * c * (d - t) * (d - t) * (d - t))/ (d * d * d * d);
        }
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        if ((t /= d / 2) < 1)
            return c / 2 * t * t * t * t + b;
        return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
    }
}
