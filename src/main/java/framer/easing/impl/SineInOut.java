package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class SineInOut implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return (Math.PI * c * Math.sin((Math.PI * t)) / (d)) / (2* d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
    }
}
