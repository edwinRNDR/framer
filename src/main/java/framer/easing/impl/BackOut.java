package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class BackOut implements Easer {


    @Override
    public double velocity(double t, double b, double c, double d) {
        return (c *(4.70158* d * d -12.8063* d * t +8.10474* t * t))/(d * d * d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        double s = 1.70158;
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    }
}
