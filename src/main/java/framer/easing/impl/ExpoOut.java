package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class ExpoOut implements Easer {

    @Override
    public double velocity(double t, double b, double c, double d) {
        if (t == d) {
            return 0;
        }
        else {
            return (c * Math.log(32) * Math.pow(2, (1-(10* t)/ d))) / d;
        }
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
    }
}
