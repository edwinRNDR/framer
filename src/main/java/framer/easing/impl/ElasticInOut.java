package framer.easing.impl;

import framer.easing.Easer;

import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class ElasticInOut implements Easer {

    @Override
    public double velocity(double t, double b, double c, double d) {

        if (t /(d /2) < 1) {
            return (c * pow(2,((20* t)/ d)) * (0.0160887 * sin((2*20.944 * t)/ d)+0.0143284 * cos((2*20.944 * t)/ d)))/ d;

        } else {
            return (pow(2,(-(20* t)/ d)) * (20.944 * c * cos(1.5708-(20.944 * t)/ d)-6.93147 * c * sin((20.944 * (t -0.075 * d))/ d)))/ d;
        }

    }

    @Override
    public double ease(double t, double b, double c, double d) {
        if (t == 0)
            return b;
        if ((t /= d / 2) == 2)
            return b + c;
        double p = d * (.3f * 1.5f);
        double a = c;
        double s = p / 4;
        if (t < 1)
            return -.5f * (a * pow(2, 10 * (t -= 1)) * sin((t * d - s) * (2 * Math.PI) / p)) + b;
        return a * pow(2, -10 * (t -= 1)) * sin((t * d - s) * (2 * Math.PI) / p) * .5f + c + b;


    }
}
