package framer.easing.impl;

import framer.easing.Easer;

public class QuadOut implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return (-2* c *(d - t))/(d * d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return -c * (t /= d) * (t - 2) + b;
    }
}
