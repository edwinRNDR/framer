package framer.easing.impl;

import framer.easing.Easer;

import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class ElasticIn implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return (c * pow(2,((10* t)/ d)) * (0.0160887 * sin((20.944 * t)/ d)+0.0143284 * cos((20.944 * t)/ d)))/ d;
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        if (t == 0) {
            return b;
        }
        else if ((t /= d) == 1) {
            return b + c;
        }
        else {
            double p = d * .3f;
            double s = p / 4;
            return -(c * pow(2, 10 * (t -= 1)) * sin((t * d - s) * (2 * Math.PI) / p)) + b;

//-(c * pow(2, 10 * (t/d - 1)) * sin(((t/d-1) * d - (d*0.3)/4) * (2 * PI) / (d*0.3))) + b;
        }
    }
}
