package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/7/14.
 */
public class BackInOut implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        if (t / (d /2) < 1) {
            return (c * t *(43.1389* t -10.3796* d))/(d * d * d);
        }
        else {
            return (c *(32.7593* d * d -75.8982* d * t +43.1389* t * t))/(d * d * d);
        }
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        double s = 1.70158;
        if ((t /= d / 2) < 1)
            return c / 2 * (t * t * (((s *= (1.525f)) + 1) * t - s)) + b;
        return c / 2 * ((t -= 2) * t * (((s *= (1.525f)) + 1) * t + s) + 2) + b;
    }
}
