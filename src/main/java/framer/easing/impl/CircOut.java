package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class CircOut implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return (c * (d - t))/(d * d * Math.sqrt((t *(2* d - t))/(d * d)));
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    }
}
