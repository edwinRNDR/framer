package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class CircIn implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return  (c * t)/(d * d * Math.sqrt(-(t * t)/(d * d)));
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
    }
}
