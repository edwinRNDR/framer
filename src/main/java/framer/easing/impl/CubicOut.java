package framer.easing.impl;

import framer.easing.Easer;

public class CubicOut implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return (3 * c * (d-t) * (d-t)) / (d*d*d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return c * ((t = t / d - 1) * t * t + 1) + b;
    }
}
