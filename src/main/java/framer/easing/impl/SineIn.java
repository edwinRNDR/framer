package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class SineIn implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return (-Math.PI * c * Math.sin((Math.PI * t) / (2* d))) / (2* d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
    }
}
