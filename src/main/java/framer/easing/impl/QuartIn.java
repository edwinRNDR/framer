package framer.easing.impl;

import framer.easing.Easer;

/**
 * Created by voorbeeld on 8/6/14.
 */
public class QuartIn implements Easer {
    @Override
    public double velocity(double t, double b, double c, double d) {
        return 4 * c * (t * t * t) / (d * d * d * d);
    }

    @Override
    public double ease(double t, double b, double c, double d) {
        double n = t / d;
        return c * n * n * n * n + b;
    }
}
