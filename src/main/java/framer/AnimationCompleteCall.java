package framer;

class AnimationCompleteCall implements Call {
	final AnimationCompletedCallback callback;
	final Animatable target;

	AnimationCompleteCall(AnimationCompletedCallback callback, Animatable target) {
		this.callback = callback;
		this.target = target;
	}

	public void call() {
		this.callback.animationCompleted(target);
	}
}
