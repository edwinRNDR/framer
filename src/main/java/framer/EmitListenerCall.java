package framer;

class EmitListenerCall implements Call{

    Animatable animatable;
    Object message;
    EmitListener listener;
    EmitListenerCall(EmitListener listener, Animatable animatable, Object message) {
        this.listener = listener;
        this.animatable = animatable;
        this.message = message;
    }

    @Override
    public void call() {
        listener.onEmit(animatable, message);
    }
}
