package framer;


class AnimationProgressCall implements Call {

	final AnimationProgressCallback callback;
	final Animatable target;
	final double time;
	final double duration;

	public AnimationProgressCall(AnimationProgressCallback callback, Animatable target, double time, double duration) {
		this.callback = callback;
		this.target = target;
		this.time = time;
		this.duration = duration;
	}


	@Override
	public void call() {
	    callback.animationProgress(target, time, duration);
	}
}
