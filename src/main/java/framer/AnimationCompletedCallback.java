package framer;

/**
 * Callback interface to report animation completion.
 * @author Edwin Jakobs
 *
 */
public interface AnimationCompletedCallback<T extends Animatable<T>> {
	/**
	 * called when the animation is completed
	 * @param target the Animatable that has completed the animation
	 */
	public  void animationCompleted(Animatable<T> target);
}
