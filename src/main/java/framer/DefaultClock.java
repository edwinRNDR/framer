package framer;

/**
 * Default clock implementation
 */
public class DefaultClock implements Clock {
    @Override
    public long time() {
        return System.currentTimeMillis();
    }
}
