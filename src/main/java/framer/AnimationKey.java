package framer;

import framer.easing.Easer;

import java.util.ArrayList;

/**
 * Animation key class
 * @author Edwin Jakobs
 */

class AnimationKey {

	final String variable;
	final double target;
	final long duration;
	long start;
	double from;

    Object message;
    String stage;

	ArrayList<AnimationCompletedCallback> completionCallbacks = new ArrayList<AnimationCompletedCallback>();
	ArrayList<AnimationProgressCallback> progressCallbacks = new ArrayList<AnimationProgressCallback>();
	ArrayList<AnimationCheckpoint> checkpointCallbacks = new ArrayList<AnimationCheckpoint>();

    public void addCheckpointCallback(long time, AnimationCompletedCallback callback) {

        AnimationCheckpoint cp = new AnimationCheckpoint();
        cp.time = time;
        cp.callback = callback;
        checkpointCallbacks.add(cp);
    }

    public enum AnimationMode {
		Blend,
		Additive
	}

	public enum AnimationState {
		Queued,
		Playing,
		Stopped
	}

	private AnimationState animationState = AnimationState.Queued;
	private AnimationMode animationMode = AnimationMode.Blend;
	private Easer easing = Easing.None.easer;

	public AnimationKey(String variable, double target, long duration, long start) {
		this.variable = variable;
		this.target = target;
		this.duration = duration;
		this.start = start;
	}

	/**
	 * sets the easing mode for the animation
	 * @param easer the easing mode
	 */
	public void setEasing(Easer easer) {
		this.easing = easer;
	}

	/**
	 * returns the easing mode
	 * @return the easing mode used
	 */
	public Easer getEasing() {
		return easing;
	}

	public void setAnimationMode(AnimationMode animationMode) {
		this.animationMode = animationMode;
	}

	public AnimationMode getAnimationMode() {
		return animationMode;
	}

	public String getVariable() {
		return variable;
	}

	/**
	 * plays the animation
	 * @param from the current value of the variable to animate
	 */
	public void play(double from) {
		this.from = from;
		animationState = AnimationState.Playing;
	}

	/**
	 * stops the animation
	 */
	public void stop() {
		animationState = AnimationState.Stopped;
	}

	/**
	 * returns all completion callbacks for the animation
	 * @return a list containing completion callbacks
	 */
	public ArrayList<AnimationCompletedCallback> getCompletionCallbacks() {
		return completionCallbacks;
	}
	
	public AnimationState getAnimationState() {
		return animationState;
	}

	/**
	 * adds a completion callback that is called when the animation ends
	 * @param callback the callback to be executed
	 */
	public void addCompletionCallback(AnimationCompletedCallback callback) {
		completionCallbacks.add(callback);
	}

	public ArrayList<AnimationProgressCallback> getProgressCallbacks() {
		return progressCallbacks;
	}
	
	public void addProgressCallback(AnimationProgressCallback callback) {
		progressCallbacks.add(callback);
	}
	
	
}

