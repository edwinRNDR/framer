package framer;

/*
    Listener for emit() commands
 */
public interface EmitListener {
    public void onEmit(Animatable animatable, Object message);
}
