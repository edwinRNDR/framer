package framer;

import framer.easing.Easer;
import framer.easing.impl.*;

/**
 * Easing enumeration for built-in easings
 * @author Edwin Jakobs
 *
 */
public enum Easing {
	/** Do not use easing */
	None(new Linear()),
	BackIn(new BackIn()),
	BackOut(new BackOut()),
	BackInOut(new BackInOut()),

	/** Bounce ease in. */
	BounceIn(new BounceIn()),
	/** Bounce ease out. */
	BounceOut(new BounceOut()),
	/** Bounce ease in and out. */
	BounceInOut(new BounceInOut()),

    CircIn(new CircIn()),
    CircOut(new CircOut()),
    CircInOut(new CircInOut()),

	/** Cubic ease in. */
	CubicIn(new CubicIn()),
	/** Cubic ease out. */
	CubicOut(new CubicOut()),
	/** Cubic ease in and out. */
	CubicInOut(new CubicInOut()),

	/** Elastic ease in. */
	ElasticIn(new ElasticIn()),
	/** Elastic ease out. */
	ElasticOut(new ElasticOut()),
	/** Elastic ease in and out. */
	ElasticInOut(new ElasticInOut()),
	
	ExpoIn(new ExpoIn()),
	ExpoOut(new ExpoOut()),
	ExpoInOut(new ExpoInOut()),

	/** Quadratic ease in. */
	QuadIn(new QuadIn()),
	/** Quadratic ease out. */
	QuadOut(new QuadOut()),
	/** Quadratic ease in and out. */
	QuadInOut(new QuadInOut()),
	
	QuartIn(new QuartIn()),
	QuartOut(new QuartOut()),
	QuartInOut(new QuartInOut()),

    QuintIn(new QuintIn()),
	QuintOut(new QuintOut()),
	QuintInOut(new QuintInOut()),
	
	/** Sine ease in. */
	SineIn(new SineIn()),
	/** Sine ease out. */
	SineOut(new SineOut()),
	/** Sine ease in and out. */
	SineInOut(new SineInOut());

    Easer easer;
    Easing(Easer easer) {
        this.easer = easer;
    }
}