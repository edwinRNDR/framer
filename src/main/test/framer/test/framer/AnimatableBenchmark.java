package framer;

import org.junit.Test;

/**
 * Created by voorbeeld on 3/17/16.
 */
public class AnimatableBenchmark {

    public static class A extends Animatable {
        double v = 0.0;
    }

    @Test
    public void animateBenchMark() {
        A[] as = new A[1000000];

        for (int i = 0; i < as.length; ++i) {
            as[i] = new A();
        }

        long start = System.currentTimeMillis();
        for (int i = 0; i < as.length; ++i) {
            as[i].updateAnimation(start);
            as[i].animate("v", 1.0, 1000);
        }
        long end0 = System.currentTimeMillis();
        System.out.println("that took: " + (end0-start));


        for (int k = 0; k <40 ;++k)
            for (int i = 0; i < as.length; ++i) {
                as[i].updateAnimation(start+10+k);
            }
        long end = System.currentTimeMillis();
        System.out.println("that took: " + (end-end0));
    }
}
