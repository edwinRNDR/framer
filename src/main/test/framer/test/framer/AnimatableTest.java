package framer;

import framer.Animatable;
import framer.AnimationCompletedCallback;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by voorbeeld on 1/26/14.
 */

public class AnimatableTest {

    @Test
    public void testComplete() {
        class A extends Animatable {
            A() { super(0); }
            public double x;
            public double y;
        };

        A a = new A();
        a.updateAnimation(0);
        a.animate("x", 1.0, 100);
        a.animate("y", 1.0, 400);
        a.complete("x");
        assertEquals(100, a.createAtTime);
        a.complete("y");
        assertEquals(400, a.createAtTime);
        a.complete("x");
        assertEquals(100, a.createAtTime);
        a.complete("this_does_not_exist");
        assertEquals(100, a.createAtTime);


    }


    @Test
    public void testDuration() {
        class A extends Animatable {
            A() { super(0); }
            public double x;
            public double y;
        };
        A a = new A();
        a.updateAnimation();
        a.animate("x", 1.0, 100);
        assertEquals(100, a.duration());
    }

    @Test
    public void testBasicAnimation() {

        class A extends Animatable {
            public double x;
        };

        A a = new A();
        a.updateAnimation();
        a.animate("x", 1.0, 100);

        while (a.hasAnimations()) {
            a.updateAnimation();
        }

        assertEquals(1.0, a.x, 0.00001);
    }

    @Test
    public void testEmission() {
        Animatable a = new Animatable();

        class Listener implements  EmitListener {
            boolean end;
            int step = 0;
            @Override
            public void onEmit(Animatable animatable, Object message) {

                if (message.equals("M1")) {
                    assertEquals(0, step);
                    step++;
                }
                else if (message.equals("M2")) {
                    assertEquals(1, step);
                    end = true;
                    step++;
                }
            }
        }
        Listener listener = new Listener();
        a.listen(listener);
        a.updateAnimation();
        a.emit("M1").delay(200).emit("M2");
        a.updateAnimation();
        while (!listener.end) {
            a.updateAnimation();;
        }
        assertEquals(2, listener.step);

    }

    @Test
    public void testStages() {

        Animatable a  = new Animatable();
        a.updateAnimation();

        a.stage("onset").animate("x", 1, 1000).complete(new AnimationCompletedCallback() {
            @Override
            public void animationCompleted(Animatable target) {
                assertEquals("onset", target.stage());
            }
        });
        a.stage("wait").delay(1000).stage("offset").animate("x", 0, 1000).complete(new AnimationCompletedCallback() {
            @Override
            public void animationCompleted(Animatable target) {
                assertEquals("offset", target.stage());
            }
        });

        while (a.hasAnimations()) {
            a.updateAnimation();
            //System.out.println(a.stage());
        }
    }

    public static class TestAnim extends Animatable {
        public double x;
    }

    @Test
    public void testVelocity() {
        TestAnim a = new TestAnim();

        a.updateAnimation();
        a.animate("x", 1, 1000);

        assertEquals(1.0, a.velocity("x"), 0.001);

    }

    @Test
    public void testAfterStart() {
        TestAnim a = new TestAnim();
        a.updateAnimation();


        a.animate("x", 1, 1000).afterStart(500, new AnimationCompletedCallback() {
            @Override
            public void animationCompleted(Animatable target) {

            }
        });

        while (a.hasAnimations()) {
            a.updateAnimation();;
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }

    @Test
    public void testHasAnimations() {
        TestAnim a = new TestAnim();
        a.animate("a", 1, 1000);

        assertTrue(a.hasAnimations());
        assertTrue(a.hasAnimations("a"));
        assertFalse(a.hasAnimations("b"));
        assertTrue(a.hasAnimations("a", "b"));
        a.cancel();

        assertFalse(a.hasAnimations());
        assertFalse(a.hasAnimations("a"));
    }





    @Test
    public void testBeforeComplete() {
        TestAnim a = new TestAnim();
        a.updateAnimation();


        a.animate("x", 1, 1000).beforeComplete(10, new AnimationCompletedCallback() {
            @Override
            public void animationCompleted(Animatable target) {

            }
        }).complete(new AnimationCompletedCallback() {
            @Override
            public void animationCompleted(Animatable target) {

            }
        });

        while (a.hasAnimations()) {
            a.updateAnimation();;
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }

}
