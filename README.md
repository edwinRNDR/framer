# Framer #

Framer is a simple but effective Java 7 library for interactive animations. 

# Processing version #

A version of Framer for Processing can be downloaded [here](https://bitbucket.org/edwinlust/framer/downloads/framer-0.1.7-processing.zip)

# Documentation #

A tutorial that describes basic use can be found in the [wiki](https://bitbucket.org/edwinlust/framer/wiki/Tutorial_BasicAnimation)